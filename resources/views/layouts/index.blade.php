<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title></title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700&amp;subset=cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="{{ config('app.locale') }}">

    <section class="top">
        <div class="light"></div>
        <div>
            <header>
                <div class="wrapper">
                    <a class="logo" href=""><img src="images/logo.png" class="logo" alt="G&V Expo"/></a>

                    @yield('navbar')

                </div>
            </header>

            @yield('top')

            <div class="scroll">
                <img src="images/scroll.png" alt="scroll"/>
            </div>
        </div>
    </section>

    <section class="video_player">
        <div class="wrapper">
            <div class="video">


                <div class="videoWrapper videoWrapper169 js-videoWrapper">
                    <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" autoplay
                            allowfullscreen
                            data-src="https://www.youtube.com/embed/-ilOJoMhShw?autoplay=1& modestbranding=1&rel=0&hl=sv"></iframe>
                    <button class="videoPoster js-videoPoster" style="background-image:url(images/poster.jpg);">Play video
                    </button>
                </div>


            </div>

        </div>
    </section>

    <section class="how_it_works">

        @yield('how_it_works')

    </section>

    <section class="kmts_roadmap">

        {{--<section class="kmts roadmap">--}}
            {{--<div class="wrapper">--}}
                {{--<div class="kmts_inner">--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        @yield('roadmap')

    </section>

    <section class="our_app">
        <div class="wrapper">
            <div class="owl-carousel">

                @yield('carousel')

            </div>
        </div>
    </section>

    <section class="team">
        <div class="wrapper">

            @yield('team')

        </div>
    </section>

    <section class="faq" id="faq">
        <div class="wrapper">

            @yield('faq')

        </div>
    </section>

    <footer>
        <div class="wrapper">
            <ul>
                <li><a href="mailto:info@showmebiz.pro"><span>Contact us:</span> info@showmebiz.pro</a>  </li>
                <li><a href=""><img src="images/footer_icon.png"> ShowMeBiz Ltd.</a> </li>
                <li><a href="" target="_blank"><img src="images/footer_icon.png"> Investment-bonus offer  </a> </li>
                <li><a href="" target="_blank"><img src="images/footer_icon.png"> Privacy Policy</a> </li>
                <li><a href="" target="_blank"><img src="images/footer_icon.png"> Token sale Agreement</a> </li>
            </ul>
        </div>
    </footer>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
